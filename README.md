# surf (eXkc build) - simple webkit-based browser

surf is a simple Web browser based on WebKit/GTK+.
 
 it has adblock
 it support bookmark via buku

# Requirements

In order to build surf you need GTK+ and Webkit/GTK+ header files.

In order to use the functionality of the url-bar and bookmark, also install [dmenu](https://gitlab.com/eXkc/dmenu-eXkc/).

In order to use the functionality of bookmark, also install buku.

# Installation

Edit config.mk to match your local setup (surf is installed into
the /usr/local namespace by default).

Afterwards enter the following command to build and install surf (if
necessary as root):
````
# sudo make clean install
````
Running surf
run
````
# surf [URI]
````
Running surf in tabbed
run

````
# surf-tabbed

````
